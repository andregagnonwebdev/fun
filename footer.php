<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fun
 */

?>


<footer>

	<div class="container-fluid footer">
		<div class="container">
			<div id="colophon" class="site-footer row" role="contentinfo">
				<div class="site-social col-xs-12 text-center">
					<?php if ( $s = fun_get_theme_mod( 'fun-email')): ?>
						<a href="mailto:<?php echo $s ?>" target="_blank"><i class="fa fa-envelope"></i></a>
					<?php endif; ?>
					<?php if ( $s = fun_get_theme_mod( 'fun-linked-in')): ?>
							<a href="<?php echo $s ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
					<?php endif; ?>
					<?php if ( $s = fun_get_theme_mod( 'fun-twitter')): ?>
							<a href="<?php echo $s ?>" target="_blank"><i class="fa fa-twitter"></i></a>
					<?php endif; ?>
				</div><!-- .site-info -->
			</div>
		</div>
	</div>

	<div class="container-fluid footer-2">
		<div class="container">
			<div id="colophon" class="site-footer row" role="contentinfo">
				<div class="site-info col-xs-12 col-sm-6 text-center">
					<?php if ( $s = fun_get_theme_mod( 'fun-company-name')): ?>
						<div class="copyright"><?php echo $s ?></div>
					<?php endif; ?>
					<?php if ( $s = fun_get_theme_mod( 'fun-address')): ?>
						<div class="copyright"><?php echo $s ?></div>
					<?php endif; ?>
					<?php if ( 0 && $s = fun_get_theme_mod( 'fun-telephone')): ?>
						<div class="copyright"><a href="tel:<?php echo $s ?>"><?php echo $s ?></a></div>
					<?php endif; ?>
				</div>
				<div class="site-info col-xs-12 col-sm-6 text-center">
					<?php if ( $s = fun_get_theme_mod( 'fun-email')): ?>
						<div class="copyright"><a href="mailto:<?php echo $s ?>"><?php echo $s ?></a></div>
					<?php endif; ?>

					<?php if ( $s = fun_get_theme_mod( 'fun-copyright-message')): ?>
						&nbsp;<br>
						<div class="copyright"><?php echo $s ?></div>
					<?php endif; ?>
				</div><!-- .site-info -->

			</div>
		</div>
	</div>

</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
