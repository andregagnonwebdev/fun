<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package inboston
 */
?>
<?php $color = get_sub_field( 'background_color'); ?>
<?php $style = ( $color ) ? ' style="background-color:'.$color.'" ' : ' '; ?>
<?php $image = get_sub_field( 'background_image'); ?>
<?php $style = ( $image ) ? ' style="background-image: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('.$image.')" ' : $style; ?>
<div class="container-fluid container-with-background" <?php echo $style; ?>>
	<div class="container layout-hero-with-background">
		<div class="row content-area">
			<div  class="site-main" role="main">
				<article <?php post_class(); ?>>
						<div class="entry-content col-xs-12 text-center">
							<h1 class="title"><?php	echo get_sub_field( 'title');	?></h1>
              <p class="copy"><?php	echo get_sub_field( 'copy');	?></p>
            </div><!-- .entry-content -->
				</article><!-- #post-## -->
			</div>
		</div>
	</div>
</div>
