<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fun
 */

?>


			<div  class="site-main" role="main">
				<article <?php post_class(); ?>>
					<?php if ( 0 && !is_single() ) : ?>
						<hr />
					<?php endif; ?>

					<div class="col-xs-12 col-md-6">
						<div class="card">


						<header class="entry-header">
							<?php
								the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );

							if ( 0 && 'post' === get_post_type() ) :
							?>
							<div class="entry-meta">
								<?php fun_posted_on(); ?>
							</div><!-- .entry-meta -->
							<?php	endif; ?>
							<p><?php echo get_field( 'sub_title') ?><p>
						</header><!-- .entry-header -->

						<div class="entry-content">
							<?php if ( has_post_thumbnail( )): ?>
									<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
										<div class="text-center">
											<?php the_post_thumbnail( 'portfolio-size' ) ?>
										</div>
									</a>
							<?php endif; ?>
						</div>

						<footer class="entry-footer">
							<?php
								wp_link_pages( array(
								 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'fun' ),
								 'after'  => '</div>',
								) );
							?>
							<?php //fun_entry_footer(); ?>
						</footer><!-- .entry-footer -->
						<!-- <hr class="visible-xs visible-sm" /> -->
					</div>
					</div>
				</article><!-- #post-## -->
			</div>
