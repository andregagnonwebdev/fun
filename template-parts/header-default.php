<?php
/**
 * Template part for displaying header
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fun
 */

?>

<div class="container-fluid header">
	<div class="container">
		<header id="masthead" class="site-header row" role="banner">


			<div class="site-branding col-xs-12">
				<?php
				if ( is_front_page() && is_home() ) : ?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php else : ?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php

				endif; ?>
			</div><!-- .site-branding -->

      <div class="col-xs-12">
  			<nav class="navbar navbar-default" role="navigation">
  				<div class="navbar-header">
  					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
  						<span class="sr-only">Toggle navigation</span>
  						<span class="icon-bar"></span>
  						<span class="icon-bar"></span>
  						<span class="icon-bar"></span>
  					</button>
  				</div>
  				<?php
  					$menu_args = array(
  					"theme_location" => "primary",
  					"container_class" => "navbar-collapse collapse",
  					"menu_class" => "nav navbar-nav",
  					"menu_id" => "main-primary-menu",
  					);
  					wp_nav_menu( $menu_args);
  				?>
  			</nav>
      </div>
			<div class="col-xs-12">
				<?php get_search_from(); ?>
			</div>
		</header>
	</div><!-- end container -->
</div><!-- end container-fluid -->
