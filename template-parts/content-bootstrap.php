<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fun
 */

?>
<div class="container-fluid">
	<div class="container content-bootstrap">
		<div class="row content-area">
			<div  class="site-main" role="main">
				<article <?php post_class(); ?>>

					<header class="entry-header col-xs-12 col-sm-12">
						<?php
						if ( is_single() ) :
							the_title( '<h2 class="entry-title">', '</h2>' );
						else :
							the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
						endif;

						if ( 'post' === get_post_type() ) : ?>
						<div class="entry-meta">
							<?php fun_posted_on(); ?>
						</div><!-- .entry-meta -->
						<?php
						endif; ?>
					</header><!-- .entry-header -->

						<div class="entry-content col-xs-12 col-sm-12">
							<?php
							the_content();


							// wp_link_pages( array(
							// 	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'fun' ),
							// 	'after'  => '</div>',
							// ) );
							?>


            </div><!-- .entry-content -->

						<footer class="entry-footer  col-xs-12 col-sm-12">
							<?php fun_entry_footer(); ?>
							<?php if ( !is_single() ) : ?>
								<hr />
							<?php endif; ?>
						</footer><!-- .entry-footer -->
				</article><!-- #post-## -->
			</div>
		</div>
	</div>
</div>
