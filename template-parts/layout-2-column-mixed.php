<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package inboston
 */
?>
<?php $f = get_sub_field( 'background_color'); ?>
<?php $bkgnd = ( $f ) ? ' style="background-color:'.$f.'" ' : ''; ?>
<div class="container-fluid two-column-mixed" <?php echo $bkgnd; ?>>
	<div class="container">
		<div class="row content-area">
			<div  class="site-main" role="main">
				<article <?php post_class(); ?>>
						<?php
						if( have_rows('column') ):
			     		/// loop through the rows of data
							$i = 0;
						  while ( have_rows('column') ) : the_row();
						?>
							<div class="entry-content col-xs-12 col-md-6">


								<div class="">
									<?php if( $t = get_sub_field( 'title')): ?>
											<h1><?php echo $t; ?></h1>
									<?php endif; ?>
									<?php if( $t = get_sub_field( 'subtitle')): ?>
											<h2><?php echo $t; ?></h2>
									<?php endif; ?>
									<?php $image = get_sub_field( 'image'); ?>
									<?php //var_dump( $image); ?>
									<?php $image = $image['sizes']['medium']; ?>
									<div class="image text-center">
										<img class="img-responsive" src="<?php echo $image; ?>" alt="">
									</div>

									<?php if( $t = get_sub_field( 'copy')): ?>
											<p><?php echo $t; ?></p>
									<?php endif; ?>

									<?php if( $l = get_sub_field( 'link')): ?>
										<?php $l = get_permalink( $l->ID); ?>
										&nbsp;<br  />
									<?php endif; ?>

									<?php if( $t = get_sub_field( 'link_title')): ?>
											<a class="link" href="<?php echo $l; ?>"><?php echo $t; ?></a>
									<?php endif; ?>

									<?php $i++; ?>
									<?php if( $i > 0 && $i % 2 == 0): ?>
										<hr class="visible-xs"/>
									<?php endif; ?>

								</div>
							</div>

	<?php
							endwhile;
						endif;
	?>
				</article><!-- #post-## -->
			</div>
		</div>
	</div>
</div>
