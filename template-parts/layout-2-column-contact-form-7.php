<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package inboston
 */
?>
<?php $f = get_sub_field( 'background_color'); ?>
<?php $bkgnd = ( $f ) ? ' style="background-color:'.$f.'" ' : ''; ?>
<div class="container-fluid two_column_contact_form_7" <?php echo $bkgnd; ?>>
	<div class="container">
		<div class="row content-area">
			<div  class="site-main" role="main">
				<article <?php post_class(); ?>>
						<div class="entry-content col-xs-12 col-md-5">
              <?php	echo get_sub_field( 'column_1');	?>
            </div><!-- .entry-content -->
            <div class="entry-content col-xs-12 col-md-7">
							<?php	if( $f = get_sub_field( 'form_id')): ?>
								<?php echo do_shortcode( '[contact-form-7 id="'.$f.'" title="Form"]' );	?>
							<?php endif; ?>
            </div><!-- .entry-content -->
				</article><!-- #post-## -->
			</div>
		</div>
	</div>
</div>
