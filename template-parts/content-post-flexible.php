<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fun
 *
 * Not used.
 */

?>

<div id="main-content">
  <main role="main">

		<?php
		// check if the flexible content field has rows of data
		if( have_rows('flexible_layout') ):

		     // loop through the rows of data
		    while ( have_rows('flexible_layout') ) : the_row();

		        switch( get_row_layout()) {
              case 'hero':
								get_template_part( 'template-parts/layout-hero' );
							break;
              case 'hero_with_background':
								get_template_part( 'template-parts/layout-hero-with-background' );
							break;
              case 'title_and_meta':
								get_template_part( 'template-parts/layout-title-and-meta' );
							break;
							case '1_column':
								get_template_part( 'template-parts/layout-1-column' );
								break;
							case '2_column':
								get_template_part( 'template-parts/layout-2-column' );
							break;
							case '2_column_mixed':
								get_template_part( 'template-parts/layout-2-column-mixed' );
								break;
							case 'horizontal_rule':
								get_template_part( 'template-parts/layout-horizontal-rule' );
								break;
		          default:
			        	break;
			      }
			  endwhile;


			else :

			    // no layouts found
          get_template_part( 'template-parts/content', 'bootstrap' );
				//	get_template_part( 'template-parts/layout-default' );

			endif;
			?>


	</main>
</div>
