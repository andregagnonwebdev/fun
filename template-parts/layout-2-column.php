<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package inboston
 */
?>
<?php $f = get_sub_field( 'background_color'); ?>
<?php $bkgnd = ( $f ) ? ' style="background-color:'.$f.'" ' : ''; ?>
<div class="container-fluid" <?php echo $bkgnd; ?>>
	<div class="container layout-2-column">
		<div class="row content-area">
			<div  class="site-main" role="main">
				<article <?php post_class(); ?>>
						<div class="entry-content col-xs-12 col-md-6">
              <?php	echo get_sub_field( 'col1');	?>
            </div><!-- .entry-content -->
            <div class="entry-content col-xs-12 col-md-6">
              <?php	echo get_sub_field( 'col2');	?>
            </div><!-- .entry-content -->
				</article><!-- #post-## -->
			</div>
		</div>
	</div>
</div>
