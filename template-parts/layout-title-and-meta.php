<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package inboston
 */
?>
<?php $color = get_sub_field( 'background_color'); ?>
<?php $style = ( $color ) ? ' style="background-color:'.$color.'" ' : ' '; ?>
<?php $image = get_sub_field( 'background_image'); ?>
<?php $style = ( $image ) ? ' style="background-image: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('.$image.')" ' : $style; ?>
<div class="container-fluid container-with-background" <?php echo $style; ?>>
	<div class="container layout-hero-with-background layout-title-and-meta">
		<div class="row content-area">
			<div  class="site-main" role="main">
					<header class="entry-header text-center col-xs-12 col-sm-12">
						<?php
						if ( is_single() ) :
							the_title( '<h1 class="entry-title title">', '</h2>' );
						else :
							the_title( '<h1 class="entry-title title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
						endif;

						if ( 'post' === get_post_type() ) : ?>
						<div class="entry-meta">
							<?php fun_posted_on(); ?>
						</div><!-- .entry-meta -->
						<div class="entry-meta">
							<?php fun_entry_footer(); ?>
						</div><!-- .entry-meta -->
						<?php
						endif; ?>
					</header><!-- .entry-header -->
			</div>
		</div>
	</div>
</div>
