<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package inboston
 */
?>
<?php $f = get_sub_field( 'background_color'); ?>
<?php $bkgnd = ( $f ) ? ' style="background-color:'.$f.'" ' : ''; ?>
<?php $f = get_sub_field( 'is_a_pricing_table'); ?>
<?php $add_class = ( $f ) ? 'pricing-table' : ''; ?>

<div class="container-fluid" <?php echo $bkgnd; ?>>
	<div class="container combo-3-column <?php echo $add_class; ?>">
		<div class="row content-area">
			<div  class="site-main" role="main">
				<article <?php post_class(); ?>>
						<?php
						if( have_rows('column') ):
			     		/// loop through the rows of data
							$i = 0;
						  while ( have_rows('column') ) : the_row();
						?>
							<div class="entry-content col-xs-12 col-md-4 column fixed">
								<?php if( 0 && $i > 0): ?>
									<hr class="visible-xs"/>
								<?php endif; ?>

								<div class="text-center">
									<?php if( $t = get_sub_field( 'icon')): ?>
											<i class="icon fa fa-<?php echo $t; ?>"></i>
									<?php endif; ?>

									<?php if( $t = get_sub_field( 'title')): ?>
											<h3 class="title"><?php echo $t; ?></h3>
									<?php endif; ?>
									<?php if( $t = get_sub_field( 'subtitle')): ?>
											<h4 class="sub-title"><?php echo $t; ?></h4>
									<?php endif; ?>
									<?php $image = get_sub_field( 'image'); ?>
									<?php //var_dump( $image); ?>
									<?php $image = $image['sizes']['medium']; ?>
									<div class="image text-center">
										<img class="img-responsive" src="<?php echo $image; ?>" alt="">
									</div>

									<?php if( $t = get_sub_field( 'copy')): ?>
											<p><?php echo $t; ?></p>
									<?php endif; ?>

									<?php if( $l = get_sub_field( 'link')): ?>
										<?php $l = get_permalink( $l->ID); ?>
										<!-- &nbsp;<br  /> -->
									<?php endif; ?>

									<?php if( $t = get_sub_field( 'link_title')): ?>
											<a class="button" href="<?php echo $l; ?>"><?php echo $t; ?></a>
									<?php endif; ?>

								</div>
								<hr class="visible-xs" />
							</div>

	<?php
								$i++;
							endwhile;
						endif;
	?>
				</article><!-- #post-## -->
			</div>
		</div>
	</div>
</div>
