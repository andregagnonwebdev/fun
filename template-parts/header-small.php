<?php
/**
 * Template part for displaying header
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fun
 */

?>

<div class="container-fluid header">
	<div class="container">
		<header id="masthead" class="site-header row" role="banner">
			<div class="visible-xs visible-sm col-xs-12 tophat">
				<?php if ( $s = fun_get_theme_mod( 'fun-telephone')): ?>
					<a href="tel:<?php echo $s ?>"><i class="fa fa-phone"></i></a>
				<?php endif; ?>
				<?php if ( $s = fun_get_theme_mod( 'fun-email')): ?>
					<a href="mailto:<?php echo $s ?>" target="_blank"><i class="fa fa-envelope"></i> &nbsp;Contact</a>
				<?php endif; ?>
			</div>

			<div class="hidden-xs hidden-sm col-md-12 tophat">
				<?php if ( $s = fun_get_theme_mod( 'fun-telephone')): ?>
					<a href="tel:<?php echo $s ?>"><i class="fa fa-phone"></i></a>
				<?php endif; ?>
				<?php if ( $s = fun_get_theme_mod( 'fun-email')): ?>
					<a href="mailto:<?php echo $s ?>" target="_blank"><i class="fa fa-envelope"></i> &nbsp;Contact</a>
				<?php endif; ?>
			</div><!-- .site-branding -->

			<div class="site-branding col-xs-7 col-sm-4 col-md-3">
				<?php  if ( $s = fun_get_theme_mod( 'fun-company-name')): ?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo $s; ?></a></h1>
				<?php endif; ?>
			</div><!-- .site-branding -->


      <div class="col-xs-5 col-sm-8 col-md-9">
  			<nav class="navbar navbar-default" role="navigation">
  				<div class="navbar-header">
  					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
  						<span class="sr-only">Toggle navigation</span>
  						<span class="icon-bar"></span>
  						<span class="icon-bar"></span>
  						<span class="icon-bar"></span>
  					</button>
  				</div>
					<div class="hidden-xs hidden-sm navbar-right">
  				<?php
  					$menu_args = array(
  					"theme_location" => "primary",
  					"container_class" => "navbar-collapse collapse",
  					"menu_class" => "nav navbar-nav",
  					"menu_id" => "main-primary-menu",
//						"walker" => new Nav_SubMenu_Walker(),
  					);
  					wp_nav_menu( $menu_args);
  				?>
					</div>
  			</nav>
      </div>
			<div class="visible-xs visible-sm col-xs-12">
  			<nav class="navbar navbar-default" role="navigation">
  				<?php
  					$menu_args = array(
  					"theme_location" => "primary",
  					"container_class" => "navbar-collapse collapse",
  					"menu_class" => "nav navbar-nav",
  					"menu_id" => "main-primary-menu",
  					);
  					wp_nav_menu( $menu_args);
  				?>
  			</nav>
      </div>
		</header>
		<div class="col-xs-12 text-right" style="display:none">
			<?php get_search_form(); ?>
		</div>
	</div><!-- end container -->
</div><!-- end container-fluid -->

<div class="container-fluid search-container"  style="display:none;">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-right">
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>
</div>
