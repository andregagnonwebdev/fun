<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fun
 */

?>
<div class="container-fluid portfolio">
	<div class="container">
		<div class="row content-area">
			<div  class="site-main" role="main">
				<article <?php post_class(); ?>>
					<?php if ( !is_single() ) : ?>
						<hr />
					<?php endif; ?>


					<header class="entry-header col-xs-12 col-md-8">

						<?php if ( is_single() ) : ?>
							<h6><a href="/wordpress-website-portfolio">WEB DESIGN PORTFOLIO</a></h6>
						<?php endif; ?>

						<?php
						if ( is_single() ) :
							the_title( '<h1 class="entry-title">', '</h1>' );
						else :
							the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
						endif;

						if ( 0 && 'post' === get_post_type() ) : ?>
						<div class="entry-meta">
							<?php fun_posted_on(); ?>
						</div><!-- .entry-meta -->

						<?php	endif; ?>
						<h4><?php echo get_field( 'sub_title') ?></h4>

						<?php if ( is_single() ) : ?>
							<?php
							    $site_url = get_field('site_url');
							    if ( $field = get_field( 'url_title'))
							    {
							        // get multiple URLs
							        if ( strstr( $field, ',') != false )
							        {
							            $titles = explode( ',', $field);
							            $urls = explode( ',', get_field( 'site_url'));
							            $site_url = $urls[ 0];
							            foreach ( $titles as $key => $t)
							                echo '<a href="'.trim( $urls[$key]).'" title="'.$title.'" target="_blank" >'.trim( $t).'</a><br />';
							        }
							        else
							        {
							            $site_url = get_field('site_url');
							            echo '<strong><a href="'.$site_url.'" title="'.$title.'" target="_blank" >'.$field.'</a></strong>';
							        }
							    }
							?>
						<?php endif; ?>

						<?php
							if ( is_single() ) :
								the_content();
							endif;
						?>
					</header><!-- .entry-header -->

					<div class="entry-content col-xs-12 col-md-4">

						<?php if ( has_post_thumbnail( )): ?>
							<?php if ( is_single() ) : ?>
								<div class="text-center">
									<?php the_post_thumbnail( 'portfolio-size' ) ?>
								</div>
							<?php else: ?>
								<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
									<div class="text-center">
										<?php the_post_thumbnail( 'portfolio-size' ) ?>
									</div>
								</a>
							<?php endif; ?>
						<?php endif; ?>

						<?php if ( is_single() && $f = get_field( 'testimonial_body')): ?>
							<p class="testimonial">"<?php echo $f ?>"</p>
							<div class="testimonial-author">
								<h5><?php echo get_field( 'testimonial_author') ?></h5>
							</div>
						<?php endif; ?>
					</div>

					<footer class="entry-footer  col-xs-12 col-md-12">
						<?php
							wp_link_pages( array(
							 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'fun' ),
							 'after'  => '</div>',
							) );
						?>
						<?php //fun_entry_footer(); ?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->
			</div>
		</div>
	</div>
</div>
