<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package inboston
 */
?>
<?php $f = get_sub_field( 'background_color'); ?>
<?php $bkgnd = ( $f ) ? ' style="background-color:'.$f.'" ' : ''; ?>
<div class="container-fluid" <?php echo $bkgnd; ?>>
	<div class="container">
		<div class="row content-area">
			<div  class="site-main" role="main">
				<article <?php post_class(); ?>>
						<div class="entry-content col-xs-12 text-center">
							<h1 class="title"><?php	echo get_sub_field( 'title');	?></h1>
              <p class="copy"><?php	echo get_sub_field( 'copy');	?></p>
            </div><!-- .entry-content -->
				</article><!-- #post-## -->
			</div>
		</div>
	</div>
</div>
