<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package fun
 */

get_header(); ?>

<div id="main-content">
  <main role="main">

		<?php
		while ( have_posts() ) : the_post();

      //get_template_part( 'template-parts/content', 'bootstrap' );
			get_template_part( 'template-parts/content', 'post-flexible' );

			//the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			//if ( comments_open() || get_comments_number() ) :
			//	comments_template();
			//endif;
      ?>
      <div class="container-fluid">
        <div class="container">
          <div class="row content-area">
            <div  class="col-xs-12">
              <?php
              $args = array(
                'prev_text' => '&laquo; Previous Post',
                'next_text' => 'Next Post &raquo;',
                );
              the_post_navigation( $args);
              ?>
              &nbsp;<br />
              &nbsp;<br />
            </div>
          </div>
        </div>
      </div>
      <?php

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
