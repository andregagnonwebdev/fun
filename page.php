<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fun
 */

get_header(); ?>

<div id="main-content">
  <main role="main">

		<?php
		while ( have_posts() ) : the_post();

		// check if the flexible content field has rows of data
		if( have_rows('flexible_layout') ):
			     // loop through the rows of data
			    while ( have_rows('flexible_layout') ) : the_row();
						switch( get_row_layout()) {

              case 'hero':
								get_template_part( 'template-parts/layout-hero' );
							break;
              case 'hero_with_background':
								get_template_part( 'template-parts/layout-hero-with-background' );
							break;              
              case 'title_and_meta':
								get_template_part( 'template-parts/layout-title-and-meta' );
							break;
              case '3_column_pricing_table':
                echo '<div class="pricing-table">';
                get_template_part( 'template-parts/layout-3-column-icon-content-button' );
                echo '</div>';
								break;
							case '1_column':
								get_template_part( 'template-parts/layout-1-column' );
								break;
							case '2_column':
								get_template_part( 'template-parts/layout-2-column' );
							break;
							case '2_column_mixed':
								get_template_part( 'template-parts/layout-2-column-mixed' );
								break;
							case '2_column_form':
								get_template_part( 'template-parts/layout-2-column-form' );
							break;
              case '2_column_contact_form_7':
								get_template_part( 'template-parts/layout-2-column-contact-form-7' );
							break;
							case '2_column_map':
								get_template_part( 'template-parts/layout-2-column-map' );
								break;
              case '3_column_icon_content_button':
								get_template_part( 'template-parts/layout-3-column-icon-content-button' );
								break;
							case 'horizontal_rule':
								get_template_part( 'template-parts/layout-horizontal-rule' );
								break;
							default:
								break;
						}
				  endwhile;

		else :
				    // no layouts found
						?>
            <?php  get_template_part( 'template-parts/layout-default' ); ?>
						<!-- <div class="container-fluid">
							<div class="container">
								<div class="row content-area">
									<div class="col-xs-12">
										<?php
										//get_template_part( 'template-parts/content', 'page' );
										?>
									</div>
								</div>
							</div>
						</div> -->

						<?php

		endif;

//			get_template_part( 'template-parts/content', 'page-flexible' );

		endwhile; // End of the loop.
		?>
	</main>
</div>

<?php
get_footer();
