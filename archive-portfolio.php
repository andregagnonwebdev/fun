<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fun
 */

get_header(); ?>

<div id="main-content">
  <main role="main">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-headerx">
				<?php
					//the_archive_title( '<h1 class="page-title">', '</h1>' );
					//the_archive_description( '<div class="archive-description">', '</div>' );
				?>
        <div class="container-fluid">
        	<div class="container">
        		<div class="row content-area">
      						<div class="entry-content col-xs-12 col-sm-12">
                    <?php //var_dump( $post_type); ?>
                    <?php $p = get_page( get_page_by_path( 'wordpress-website-'.$post_type)); ?>
                    <p><?php echo wpautop($p->post_content); ?></p>
                  </div><!-- .entry-content -->
        		</div>
        	</div>
        </div>

			</header><!-- .page-header -->
      <?php
          global $query_string;
          //var_dump( $query_string);
          query_posts( $query_string . '&orderby=date&order=DESC&posts_per_page=-1' );

      ?>
      <div class="container-fluid portfolio">
      	<div class="container">
      		<div class="row content-area">
      			<?php
      			/* Start the Loop */
            $i = 0;
      			while ( have_posts() ) : the_post();
              if ( 0 && $i % 2 == 0):
              ?>
                <hr class="hidden-xs hidden-sm" />
              <?php
              endif;

      				get_template_part( 'template-parts/content', 'portfolio' );
              ?>
              <?php
              $i++;
              if ( $i % 2 == 0):
              ?>
                </div>
                <div class="row content-area">
              <?php
              endif;

      			endwhile;
      			the_posts_navigation();
            ?>
        </div>
      </div>
    </div>

    <?php
		else :
			get_template_part( 'template-parts/content', 'none' );
		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
