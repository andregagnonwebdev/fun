<?php

function portfolio_init() {
	register_post_type( 'portfolio', array(
		'labels'            => array(
			'name'                => __( 'Portfolios', 'fun' ),
			'singular_name'       => __( 'Portfolio', 'fun' ),
			'all_items'           => __( 'All Portfolios', 'fun' ),
			'new_item'            => __( 'New portfolio', 'fun' ),
			'add_new'             => __( 'Add New', 'fun' ),
			'add_new_item'        => __( 'Add New portfolio', 'fun' ),
			'edit_item'           => __( 'Edit portfolio', 'fun' ),
			'view_item'           => __( 'View portfolio', 'fun' ),
			'search_items'        => __( 'Search portfolios', 'fun' ),
			'not_found'           => __( 'No portfolios found', 'fun' ),
			'not_found_in_trash'  => __( 'No portfolios found in trash', 'fun' ),
			'parent_item_colon'   => __( 'Parent portfolio', 'fun' ),
			'menu_name'           => __( 'Portfolios', 'fun' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'       => true,
		'rewrite'           => array( 'slug' => 'wordpress-website-portfolio'),
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'taxonomies' 				=> array('category', 'post_tag'),
		'show_in_rest'      => true,
		'rest_base'         => 'portfolio',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'portfolio_init' );

function portfolio_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['portfolio'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Portfolio updated. <a target="_blank" href="%s">View portfolio</a>', 'fun'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'fun'),
		3 => __('Custom field deleted.', 'fun'),
		4 => __('Portfolio updated.', 'fun'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Portfolio restored to revision from %s', 'fun'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Portfolio published. <a href="%s">View portfolio</a>', 'fun'), esc_url( $permalink ) ),
		7 => __('Portfolio saved.', 'fun'),
		8 => sprintf( __('Portfolio submitted. <a target="_blank" href="%s">Preview portfolio</a>', 'fun'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Portfolio scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview portfolio</a>', 'fun'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Portfolio draft updated. <a target="_blank" href="%s">Preview portfolio</a>', 'fun'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'portfolio_updated_messages' );


// not used
function fun_register_taxonomy_function()
{
    $labels = [
        'name'              => _x('Functions', 'taxonomy general name'),
        'singular_name'     => _x('Function', 'taxonomy singular name'),
        'search_items'      => __('Search Functions'),
        'all_items'         => __('All Functions'),
        'parent_item'       => __('Parent Function'),
        'parent_item_colon' => __('Parent Function:'),
        'edit_item'         => __('Edit Function'),
        'update_item'       => __('Update Function'),
        'add_new_item'      => __('Add New Function'),
        'new_item_name'     => __('New Function Name'),
        'menu_name'         => __('Function'),
    ];
    $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => 'function'],
    ];
    register_taxonomy('function', ['portfolio'], $args);
}
//add_action('init', 'fun_register_taxonomy_function');
