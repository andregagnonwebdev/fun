<?php

function code_example_init() {
	register_post_type( 'code-example', array(
		'labels'            => array(
			'name'                => __( 'Code examples', 'fun' ),
			'singular_name'       => __( 'Code example', 'fun' ),
			'all_items'           => __( 'All Code examples', 'fun' ),
			'new_item'            => __( 'New code example', 'fun' ),
			'add_new'             => __( 'Add New', 'fun' ),
			'add_new_item'        => __( 'Add New code example', 'fun' ),
			'edit_item'           => __( 'Edit code example', 'fun' ),
			'view_item'           => __( 'View code example', 'fun' ),
			'search_items'        => __( 'Search code examples', 'fun' ),
			'not_found'           => __( 'No code examples found', 'fun' ),
			'not_found_in_trash'  => __( 'No code examples found in trash', 'fun' ),
			'parent_item_colon'   => __( 'Parent code example', 'fun' ),
			'menu_name'           => __( 'Code examples', 'fun' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'code-example',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'code_example_init' );

function code_example_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['code-example'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Code example updated. <a target="_blank" href="%s">View code example</a>', 'fun'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'fun'),
		3 => __('Custom field deleted.', 'fun'),
		4 => __('Code example updated.', 'fun'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Code example restored to revision from %s', 'fun'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Code example published. <a href="%s">View code example</a>', 'fun'), esc_url( $permalink ) ),
		7 => __('Code example saved.', 'fun'),
		8 => sprintf( __('Code example submitted. <a target="_blank" href="%s">Preview code example</a>', 'fun'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Code example scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview code example</a>', 'fun'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Code example draft updated. <a target="_blank" href="%s">Preview code example</a>', 'fun'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'code_example_updated_messages' );
