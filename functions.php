<?php
/**
 * FUN functions and definitions
 *
 * @package FUN
 */

define( 'LOCAL_IP_ADDR', '10.0.2.15');
define( 'DISALLOW_FILE_EDIT', true);

require get_template_directory() . '/inc/util.php';

$x = '';
//$x = $y;
/*
fun_error_log( 'TEST: line:'.__LINE__.' '.__FILE__);
fun_error_log( 'GETLOG: ' . ini_get('error_log'));
fun_error_log( 'CONTENT: ' .  WP_CONTENT_DIR);
fun_error_log( 'DEBUG: ' .  WP_DEBUG);
fun_error_log( 'DEBUG_L: ' .  WP_DEBUG_LOG);
fun_error_log( 'DEBUG_D: ' .  WP_DEBUG_DISPLAY);
fun_error_log( 'SETLOG: ' . ini_set( 'error_log', '/var/www/funnel/wp-content' . '/error.log'));
*/
//fun_error_log( 'SETLOG: ' . ini_set( 'error_log', '/var/www/funnel/wp-content' . '/error.log'));
//fun_error_log( 'GETLOG: ' . ini_get( 'error_log'));

//echo write_log( $x);

if ( ! function_exists( 'fun_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function fun_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on FUN, use a find and replace
	 * to change 'fun' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'fun', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'custom-size', 220, 180, false );
	add_image_size( 'portfolio-size', 400, 0, array( 'center', 'top') );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'fun' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'fun_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// custom logo
	add_theme_support( 'custom-logo', array(
		 'width'       => 320,
		 'height'      => 200,
	) );
}
endif; // fun_setup
add_action( 'after_setup_theme', 'fun_setup' );


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
	 * @global int $content_width
	 */
	function fun_content_width() {
		$GLOBALS['content_width'] = apply_filters( 'fun_content_width', 640 );
	}
	add_action( 'after_setup_theme', 'fun_content_width', 0 );


/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function fun_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'fun' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'fun_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function fun_scripts() {

	if ( WP_DEBUG)
		$version = '4'.date( '.YmdGi'); // for DEBUG
	else
		$version = null;

	// WP required CSS
  wp_enqueue_style( 'fun-style', get_stylesheet_uri(), array(), $version ); // style.css

	if ( 1 && WP_DEBUG) {
		wp_enqueue_style( 'underscores', get_template_directory_uri() . '/css/_s.css', array(), $version);
		// bootstrap css framework
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), $version);
		// site CSS
		$css = ( WP_DEBUG) ? '/css/main.css' : '/css/dist/main.min.css';
		wp_enqueue_style( 'main', get_template_directory_uri() . $css, array( 'fun-style', 'underscores', 'bootstrap'), $version);
	}
	else {
		wp_enqueue_style( 'all', get_template_directory_uri() . '/css/dist/all.min.css', array(), $version);
	}
	// font icons
  wp_enqueue_style('font-awesome', 'https://opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css', array(), null);


	// load google fonts

	$fonts = fun_get_theme_fonts();
	global $fun_all_fonts;
	foreach ($fonts as $f) {
		if ( array_key_exists( $f, $fun_all_fonts) && $fun_all_fonts[ $f][ 1]) {
			$l = 'https://fonts.googleapis.com/css?family=';
			global $fun_all_fonts;
			$l .= str_replace( ' ', '+', $f) . ':' . $fun_all_fonts[ $f][ 1];
			$l .= '';
			wp_enqueue_style( 'google-font-'.str_replace( ' ', '-', $f), $l, array( 'fun-style', 'bootstrap'), null);
		}
	}

  	// move jquery to footer
  if (!is_admin()) {
        wp_deregister_script('jquery');
        // Load the copy of jQuery that comes with WordPress in to footer
        wp_register_script('jquery', '/wp-includes/js/jquery/jquery.js', false, $version, true);
        wp_enqueue_script('jquery');
	}

	// for watch in Gruntfile.js
	if ( in_array( $_SERVER['SERVER_ADDR'], array( '127.0.0.1', LOCAL_IP_ADDR)) || pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION) == 'test') {
    	wp_enqueue_script( 'livereload', '//localhost:35729/livereload.js', '', false, true );
	}

	// before theme code
	//wp_enqueue_script( 'google-maps-js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD7npQwHVpFXN-uWlwOZ4jc69ZLfCayrVQ', array(), null, true);

	if ( 1 && WP_DEBUG ) {
		$js = ( WP_DEBUG) ? 'bootstrap.js' : 'bootstrap.min.js';
		wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/'.$js, array('jquery'), $version, true);
		// before theme code
		//wp_enqueue_script( 'google-maps-js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD7npQwHVpFXN-uWlwOZ4jc69ZLfCayrVQ', array(), null, true);
		$js = ( WP_DEBUG) ? '/js/theme.js' : '/js/dist/theme.min.js';
	  wp_enqueue_script( 'fun-theme', get_template_directory_uri() . $js, array('jquery'), $version, true );
	}
	else {
		$js = '/js/dist/all.min.js';
		wp_enqueue_script( 'all', get_template_directory_uri() . $js, array('jquery'), $version, true );

	}
	// new fontawesome
	//wp_enqueue_script( 'font-awesome-js', 'https://use.fontawesome.com/c88288cbb6.js', array(), $version, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'fun_scripts' );

/**
 * Remove emoji support
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
//require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
//require get_template_directory() . '/inc/jetpack.php';
/**
 * Custom google fonts
 */
//require get_template_directory() . '/inc/google-font.php';

/**
 * Custom theme options
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Custom theme options
 */
//require get_template_directory() . '/inc/custom-theme-options.php';

/**
 * Utility functions
 */
// set up this global for some functions
$sep = '/';
$dl_filepath = dirname(dirname(dirname(dirname(__FILE__)))) . $sep . 'wp-content' . $sep . 'uploads';
//require get_template_directory() . '/inc/util.php';

/**
 * Contact Form class
 */
//require get_template_directory() . '/inc/class.contact.php';

/**
 * Custom post types
 */
require get_template_directory() . '/post-types/portfolio.php';
require get_template_directory() . '/post-types/code-example.php';

/**
 * WooCommerce Theme Support
 */


// Removes showing results in Storefront theme
//remove_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 20 );
//remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
// remove sort order in products display
//remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'fun_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'fun_theme_wrapper_end', 10);

function fun_theme_wrapper_start() {
	?>
	<div id="main-content">
	  <main role="main">
	      <div class="container-fluid">
	        <div class="container">
	          <div class="row content-area">
    					<div id="main" class="site-main col-xs-12 col-md-12" role="main">
	<?php
}

function fun_theme_wrapper_end() {
	?>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>
	<?php
}

add_action( 'after_setup_theme', 'fun_woocommerce_support' );
function fun_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}



////////////////////////////////////////////////////
// Google Maps
function fun_acf_init() {

	acf_update_setting('google_api_key', 'AIzaSyD7npQwHVpFXN-uWlwOZ4jc69ZLfCayrVQ');
}

add_action('acf/init', 'fun_acf_init');


// add acf custom fields to relevanssi search plugin excerpts
add_filter('relevanssi_excerpt_content', 'fun_acf_custom_fields_to_excerpts', 10, 3);

function fun_acf_custom_fields_to_excerpts($content, $post, $query) {

  $fields = get_field('repeater_field_name', $post->ID);
  if( $fields) {
    foreach($fields as $field) {
      $content .= " " . $field['repeater_sub_field_1'];
      $content .= " " . $field['repeater_sub_field_2'];
    }
  }

  $fields = get_field('flexible_layout', $post->ID);
    if( $fields ) {
    foreach($fields as $field) {

      if($field['acf_fc_layout'] == "2_column") {
				$content .= " " . $field['col1'];
        $content .= " " . $field['col2'];
      }
      elseif ($field['acf_fc_layout'] == "hero") {
        $content .= " " . $field['title'];
      }
			elseif ($field['acf_fc_layout'] == "2_column_mixed") {
        $content .= " " . $field['column'];
      }
			elseif ($field['acf_fc_layout'] == "2_column_form") {
        $content .= " " . $field['column_1'];
      }
			elseif ($field['acf_fc_layout'] == "2_column_map") {
        $content .= " " . $field['column_1'];
      }
    }
  }

  return $content;
}


// featured images in admin
// GET FEATURED IMAGE
function ST4_get_featured_image($post_ID) {
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'post-thumbmail');
        return $post_thumbnail_img[0];
    }
}
// ADD NEW COLUMN
function ST4_columns_head($defaults) {
    $defaults['featured_image'] = 'Featured Image';
    return $defaults;
}

// SHOW THE FEATURED IMAGE
function ST4_columns_content($column_name, $post_ID) {
    if ($column_name == 'featured_image') {
        $post_featured_image = ST4_get_featured_image($post_ID);
        if ($post_featured_image) {
            echo '<img src="' . $post_featured_image . '" />';
        }
    }
}

//add_filter('manage_posts_columns', 'ST4_columns_head');
//add_action('manage_posts_custom_column', 'ST4_columns_content', 10, 2);

///////////////////////////////////////////////////////////////////////////////
// custom shortcodes
function pre_shortcode( $atts, $content = null )
{
    $content = bs_fix_shortcodes2( $content);
    $output = ( $content) ? '<pre>'. $content . '</pre>' : '';
    return $output;
}
add_shortcode( 'pre', 'pre_shortcode' );

function swd_shortcode( $atts)
{
    $output = '<a href="http://www.meetup.com/WordpressDevSeacoast/" title="Seacoast NH WordPress Developers Meetup" target="_blank" >Seacoast NH WordPress Developers Meetup</a>';

    return $output;
}
add_shortcode( 'swd', 'swd_shortcode' );

function wpo_shortcode( $atts)
{
    $output = '<a href="http://wordpress.org/" title="WordPress.org" target="_blank" >WordPress</a>';

    return $output;
}
add_shortcode( 'wpo', 'wpo_shortcode' );

// contact form 7 wpcf7: turn off form ajax
add_filter( 'wpcf7_load_js', '__return_false' );

// remove embeds
function fun_deregister_scripts(){
 wp_dequeue_script( 'wp-embed' );
}
add_action( 'wp_footer', 'fun_deregister_scripts' );
