/**
 * FUN
 * http://underscores.me
 *
 * Theme javascript
 */


(function ($) {
  'use strict';

  	// make wp_nav_menu compatible with bootstrap dropdowns
	$( "li.menu-item-has-children" ).addClass( "dropdown" );
	$( "li.menu-item-has-children > a").addClass( "dropdown-toggle" );
	$( "li.menu-item-has-children > a").attr('data-toggle', 'dropdown');
	$( "ul.sub-menu" ).addClass( "dropdown-menu" );
	$( "ul.sub-menu" ).addClass( "dropdown-menu-left" );

  // search
  $( ".search-glass a").click( function () {
		$(".search-container").toggle( 200);
    $(".search-form .search-field").focus();
	});
  // brandig
  $( ".branding-container").hide();
  $( ".branding-container").css( 'display', 'none');


})(jQuery);
