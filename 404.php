<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package fun
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main " role="main">

<div class="container-fluid">
	<div class="container">
		<div class="row content-area">
			<div  class="site-main" role="main">
				<article <?php post_class(); ?>>
						<div class="entry-content col-xs-12 col-sm-12">

							<section class="error-404 not-found">
								<header class="page-header">
									<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'fun' ); ?></h1>
								</header><!-- .page-header -->

								<div class="page-content">
									<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'fun' ); ?></p>

									<?php
										get_search_form();

										the_widget( 'WP_Widget_Recent_Posts' );

									?>

								</div><!-- .page-content -->
							</section><!-- .error-404 -->


            </div><!-- .entry-content -->
				</article><!-- #post-## -->
			</div>
		</div>
	</div>
</div>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
