<?php
/**
 * FUN Theme Customizer
 *
 * @package FUN
 */


function fun_theme_mod_default( $key)
{
    static $fun_theme_mod_defaults = array(

      'fun-color-nav-link' => 'seagreen',
      'fun-color-text-body' => '#000000',
      'fun-color-text-link' => 'seagreen',
      'fun-color-text-link' => '#04496A',
      'fun-color-text-footer' => '#000000',
      'fun-color-rule-lines' => '#cecece',
      'fun-color-call-to-action' => 'orangered',

      'fun-color-background-body' => '#ffffff',

      'fun-font-nav-menu' =>  'Arial',
      'fun-font-logo' =>  'Georgia',
      'fun-font-title' =>  'Georgia',
      'fun-font-heading' =>  'Georgia',
      'fun-font-body-text' =>  'Arial',
      'fun-font-button' =>  'Arial',
      'fun-font-caption' =>  'Arial',
      'fun-font-footer' =>  'Arial',


      'fun-twitter' => '',
      'fun-linked-in' => '',

      'fun-company-name' => '',
      'fun-address' => '',
      'fun-town-state-zip' => '',
      'fun-telephone' => '',
      'fun-email' => '',

      'fun-copyright-message' => '© Copyright 2017. All rights reserved.',

      'fun-img-upload' => '',   // footer

    );

    if ( array_key_exists($key, $fun_theme_mod_defaults) )
        return( $fun_theme_mod_defaults[ $key]);
    else
    {
        return( '');
    }
}

function fun_get_theme_mod( $key, $default='unused')
{
    // provide defaults for 2nd parameter
    return( get_theme_mod( $key, fun_theme_mod_default( $key)));
}

// return all google fonts used in theme options, used in functions.php
function fun_get_theme_fonts() {

    $fonts = array();

    $fonts[] = fun_get_theme_mod( 'fun-font-nav-menu');
    $fonts[] = fun_get_theme_mod( 'fun-font-logo');
    $fonts[] = fun_get_theme_mod( 'fun-font-title');
    $fonts[] = fun_get_theme_mod( 'fun-font-heading');
    $fonts[] = fun_get_theme_mod( 'fun-font-body-text');
    $fonts[] = fun_get_theme_mod( 'fun-font-button');
    $fonts[] = fun_get_theme_mod( 'fun-font-caption');
    $fonts[] = fun_get_theme_mod( 'fun-font-footer');
    $fonts = array_unique( $fonts);

    return( $fonts);
}

/////////////////////////////
// new font stuff



static $fun_all_fonts = array(
   'Georgia' => array( 'serif', ''),
   'Arial' => array( 'sans-serif', ''),

   // google
   'Arvo'  => array( 'serif', '400,400i,700,700i'),
   'Cardo'  => array( 'serif', '400,400i,700,700i'),
   'EB Garamond' => array( 'serif', '400,400i,700'),
   'Gentium Basic'  => array( 'serif', '400,400i,700,700i'),
   'Libre Baskerville' => array( 'serif', '400,400i,700'),
   'Neuton' => array( 'serif', '400,400i,700'),

   'Lato'  => array( 'sans-serif', '400,400i,700,700i'),
   'Open Sans'  => array( 'sans-serif', '400,400i,700,700i'),
   'Work Sans'  => array( 'sans-serif', '400,400i,700,700i'),
   'Open Sans Condensed'  => array( 'sans-serif', '300'),
   'Raleway'  => array( 'sans-serif', '400,400i,700,700i'),
   'Roboto'  => array( 'sans-serif', '400,400i,700,700i'),
   'Roboto Slab'  => array( 'sans-serif', '400,400i,700,700i'),

 );

 static $fun_body_text_fonts = array(
   'Georgia' => array( 'serif', ''),
   'Arial' => array( 'Helvetica, sans-serif', ''),

   // google
   'Arvo'  => array( 'serif', '400,400i,700,700i'),
   'EB Garamond' => array( 'serif', '400,400i,700'),
   'Gentium Basic'  => array( 'serif', '400,400i,700,700i'),
   'Libre Baskerville' => array( 'serif', '400,400i,700'),
   'Neuton' => array( 'serif', '400,400i,700'),

   'Lato'  => array( 'sans-serif', '400,400i,700,700i'),
   'Open Sans'  => array( 'sans-serif', '400,400i,700,700i'),
   'Work Sans'  => array( 'sans-serif', '400,400i,700,700i'),
   'Raleway'  => array( 'sans-serif', '400,400i,700,700i'),
   'Roboto'  => array( 'sans-serif', '400,400i,700,700i'),

 );

 // Add Google fonts CSS to admin header
 function fun_load_fonts() {

   global $fun_all_fonts;

   $fonts = fun_get_theme_fonts();
   foreach ($fonts as $f) {
     if ( $fun_all_fonts[ $f][ 1]) {
       $l = '//fonts.googleapis.com/css?family=';
       global $fun_all_fonts;
       $l .= str_replace( ' ', '+', $f) . ':' . $fun_all_fonts[ $f][ 1];

       //$font_url = str_replace( ',', '%2C', $l);
       //$font_url = esc_url( $font_url);
       $font_url = esc_url( $l);
       add_editor_style( $font_url );
     }
   }

 }

// load font styles for admin
//add_action('wp_default_scripts', 'fun_load_fonts');
add_action('admin_enqueue_scripts', 'fun_load_fonts');
//add_action('init', 'fun_load_fonts');

 // Add Google font CSS to admin header
 function fun_font_family( $key) {

     global $fun_all_fonts;
     return( "'".$key."'".", ".$fun_all_fonts[ $key][0]);
}




/////////////////////////////
// old font stuff
function fun_google_fonts_filter( $f)
{
//    loco_print_r( $f);
    // remove some fonts
    return( $f['category'] != 'handwriting');
}

function fun_get_google_fonts( $max = 5, $all = true)
{
  $googleAllFontList = array();
  global $fun_all_fonts;
  global $fun_body_text_fonts;

  $fonts = ($all ?  ($fun_all_fonts):($fun_body_text_fonts));
  foreach( $fonts as $key => $f) {
    $googleAllFontList[ $key] = $key;
  }
  return( $googleAllFontList);
}

function fun_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function fun_customize_register( $wp_customize ) {
    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
 //   $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

    // remove some stuff
    $wp_customize->remove_control( 'header_textcolor' );
//    $wp_customize->remove_panel( 'widgets');
    $wp_customize->remove_section( 'static_front_page');

    // TBD make this data driven, for here and CSS below.

    ///////////////////////////////////////////////////////////////////////////
    // Colors
    $wp_customize->add_setting( 'fun-color-nav-link',
        array(
        'type' => 'theme_mod',
        'default' => fun_theme_mod_default( 'fun-color-nav-link'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'fun-color-nav-link',
        array(
        'label' => __( 'Navigation Menu', 'fun_textdomain' ),
        'section' => 'colors',
    ) ) );


    $wp_customize->add_setting( 'fun-color-text-body',
        array(
        'type' => 'theme_mod',
        'default' => fun_theme_mod_default( 'fun-color-text-body'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'fun-color-text-body',
        array(
        'label' => __( 'Body Text', 'fun_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'fun-color-text-link',
        array(
        'type' => 'theme_mod',
        'default' => fun_theme_mod_default( 'fun-color-text-link'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'fun-color-text-link',
        array(
        'label' => __( 'Link', 'fun_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'fun-color-text-footer',
        array(
        'type' => 'theme_mod',
        'default' => fun_theme_mod_default( 'fun-color-text-footer'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'fun-color-text-footer',
        array(
        'label' => __( 'Footer Text', 'fun_textdomain' ),
        'section' => 'colors',
    ) ) );


    $wp_customize->add_setting( 'fun-color-rule-lines',
        array(
        'type' => 'theme_mod',
        'default' => fun_theme_mod_default( 'fun-color-rule-lines'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'fun-color-rule-lines',
        array(
        'label' => __( 'Rule Lines', 'fun_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'fun-color-call-to-action',
        array(
        'type' => 'theme_mod',
        'default' => fun_theme_mod_default( 'fun-color-call-to-action'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'fun-color-call-to-action',
        array(
        'label' => __( 'Call to Action Button', 'fun_textdomain' ),
        'section' => 'colors',
    ) ) );


    ///////////////////////////////////////////////////////////////////////////
    // Fonts

    $fontListAll = fun_get_google_fonts( 25);
    $fontListBodyText = fun_get_google_fonts( 25, false);

    // section
    $wp_customize->add_section( 'fun-font-setting', array(
            'title'          => 'Fonts',
            'priority'       => 20,
            'description' => '<b>' . __( 'Select fonts.', 'fun' ) .'</b>' ,
        ) );


    $wp_customize->add_setting( 'fun-font-logo', array(
            'type'    => 'theme_mod',
            'default' => fun_theme_mod_default('fun-font-logo'),
            //            'transport' => 'postMessage',
                        'transport' => 'refresh',
        ) );

    $wp_customize->add_control( 'fun-font-logo', array(
            'label' => 'Logo',
            'section' => 'fun-font-setting',
            'settings'   => 'fun-font-logo',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );
    // setting/control
    $wp_customize->add_setting( 'fun-font-nav-menu', array(
            'type'    => 'theme_mod',
            'default' => fun_theme_mod_default('fun-font-nav-menu'),
    //            'transport' => 'postMessage',
            'transport' => 'refresh',
        ) );

    $wp_customize->add_control( 'fun-font-nav-menu', array(
            'label' => 'Navigation Menu',
            'section' => 'fun-font-setting',
            'settings'   => 'fun-font-nav-menu',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'fun-font-title', array(
            'type'    => 'theme_mod',
            'default' => fun_theme_mod_default('fun-font-title'),
            //            'transport' => 'postMessage',
                        'transport' => 'refresh',
        ) );

    $wp_customize->add_control( 'fun-font-title', array(
            'label' => 'Title',
            'section' => 'fun-font-setting',
            'settings'   => 'fun-font-title',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'fun-font-heading', array(
            'type'    => 'theme_mod',
            'default' => fun_theme_mod_default('fun-font-heading'),
            //            'transport' => 'postMessage',
                        'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'fun-font-heading', array(
            'label' => 'Heading',
            'section' => 'fun-font-setting',
            'settings'   => 'fun-font-heading',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'fun-font-body-text', array(
            'type'    => 'theme_mod',
            'default' => fun_theme_mod_default('fun-font-body-text'),
            //            'transport' => 'postMessage',
                        'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'fun-font-body-text', array(
            'label' => 'Body Text',
            'section' => 'fun-font-setting',
            'settings'   => 'fun-font-body-text',
            'type'    => 'select',
            'choices' => $fontListBodyText,
    ) );

    $wp_customize->add_setting( 'fun-font-button', array(
            'type'    => 'theme_mod',
            'default' => fun_theme_mod_default('fun-font-button'),
            'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'fun-font-button', array(
            'label' => 'Button',
            'section' => 'fun-font-setting',
            'settings'   => 'fun-font-button',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'fun-font-caption', array(
            'type'    => 'theme_mod',
            'default' => fun_theme_mod_default('fun-font-caption'),
            'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'fun-font-caption', array(
            'label' => 'Caption',
            'section' => 'fun-font-setting',
            'settings'   => 'fun-font-caption',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'fun-font-footer', array(
            'type'    => 'theme_mod',
            'default' => fun_theme_mod_default('fun-font-footer'),
            'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'fun-font-footer', array(
            'label' => 'Footer',
            'section' => 'fun-font-setting',
            'settings'   => 'fun-font-footer',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );


    ///////////////////////////////////////////////////////////////////////////
    // Social Media
    $wp_customize->add_section( 'fun-social-media-settings', array(
        'title'          => 'Social Media Settings',
        'priority'       => 160,
        'description' => '<b>' . __( 'Social media links.' ) .'</b>' ,
    ) );


    $wp_customize->add_setting( 'fun-linked-in', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'fun_sanitize_text',
    ) );
    $wp_customize->add_control( 'fun-linked-in', array(
        'label' => __( 'linked-in URL', 'loco_textdomain' ),
        'section' => 'fun-social-media-settings',
        'settings'   => 'fun-linked-in',
        'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'fun-twitter', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'fun_sanitize_text',
    ) );
    $wp_customize->add_control( 'fun-twitter', array(
        'label' => __( 'twitter URL', 'loco_textdomain' ),
        'section' => 'fun-social-media-settings',
        'settings'   => 'fun-twitter',
        'type'    => 'text',
    ) );



    ///////////////////////////////////////////////////////////////////////////
    // Footer

    $wp_customize->add_section( 'fun-copyright-message-settings', array(
            'title'          => 'Contact Information',
            'priority'       => 200,
    ) );

    $wp_customize->add_setting( 'fun-company-name', array(
            'type'    => 'theme_mod',
            'default' => 'test',
            'transport' => 'postMessage',
            'sanitize_callback' => 'fun_sanitize_text',
        ) );
    $wp_customize->add_control( 'fun-company-name', array(
            'label' => 'Company Name',
            'section' => 'fun-copyright-message-settings',
            'settings'   => 'fun-company-name',
            'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'fun-address', array(
            'type'    => 'theme_mod',
            'default' => '',
            'transport' => 'postMessage',
            'sanitize_callback' => 'fun_sanitize_text',
        ) );
    $wp_customize->add_control( 'fun-address', array(
            'label' => 'Address',
            'section' => 'fun-copyright-message-settings',
            'settings'   => 'fun-address',
            'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'fun-telephone', array(
            'type'    => 'theme_mod',
            'default' => fun_theme_mod_default('fun-telephone'),
            'transport' => 'postMessage',
            'sanitize_callback' => 'fun_sanitize_text',
    ) );
    $wp_customize->add_control( 'fun-telephone', array(
            'label' => 'Telephone',
            'section' => 'fun-copyright-message-settings',
            'settings'   => 'fun-telephone',
            'type'    => 'text',
    ) );


        $wp_customize->add_setting( 'fun-email', array(
                'type'    => 'theme_mod',
                'default' => fun_theme_mod_default('fun-email'),
                'transport' => 'postMessage',
                'sanitize_callback' => 'fun_sanitize_text',
        ) );
        $wp_customize->add_control( 'fun-email', array(
                'label' => 'Email',
                'section' => 'fun-copyright-message-settings',
                'settings'   => 'fun-email',
                'type'    => 'text',
        ) );

    $wp_customize->add_setting( 'fun-copyright-message', array(
            'type'    => 'theme_mod',
            'default' => fun_theme_mod_default('fun-copyright-message'),
            'transport' => 'postMessage',
            'sanitize_callback' => 'fun_sanitize_text',
        ) );
    $wp_customize->add_control( 'fun-copyright-message', array(
            'label' => 'Copyright Text',
            'section' => 'fun-copyright-message-settings',
            'settings'   => 'fun-copyright-message',
            'type'    => 'text',
    ) );


}
add_action( 'customize_register', 'fun_customize_register' );


function fun_customize_css()
{
    // add customizer CSS to <head>
    ?>


    <style type="text/css">

        .site-branding  h1 a {
          color: <?php echo fun_get_theme_mod( 'fun-color-nav-link', '#fff' ); ?>;
          text-decoration: none;
          font-family: <?php echo fun_font_family( fun_get_theme_mod( 'fun-font-logo', 'arial,helvetica')); ?>;

        }
        .navbar-default .navbar-nav > li > a {
          color: <?php echo fun_get_theme_mod( 'fun-color-nav-link', '#fff' ); ?>;
          font-family: <?php echo fun_font_family( fun_get_theme_mod( 'fun-font-nav-menu', 'arial,helvetica')); ?>;
        }
        .navbar-default .navbar-nav > li > a:hover {
            color: <?php echo fun_get_theme_mod( 'fun-color-text-link', '#aaa' ); ?>;
        }


        .navbar-default .navbar-nav .current-menu-item a {
            color: <?php echo fun_get_theme_mod( 'fun-color-nav-link', '#aaa' ); ?>;
            border-bottom: 2px solid <?php echo fun_get_theme_mod( 'fun-color-nav-link', '#aaa' ); ?>;
        }

        .navbar .navbar-header button .icon-bar {
          background-color: <?php echo fun_get_theme_mod( 'fun-color-nav-link', '#aaa' ); ?>;
        }

        .navbar .navbar-header button .icon-title {
          color: <?php echo fun_get_theme_mod( 'fun-color-nav-link', '#aaa' ); ?>;
        }



        input[type="submit"] {
            font-family: <?php echo fun_font_family( fun_get_theme_mod( 'fun-font-nav-menu', 'arial,helvetica')); ?>;
        }
        button.search-submit {
          background-color:  <?php echo fun_get_theme_mod( 'fun-color-text-link', '#aaa' ); ?>;
          color: white;
        }

        body, #main {
            color: <?php echo fun_get_theme_mod( 'fun-color-text-body', '#000' ); ?>;
            font-family: <?php echo fun_font_family( fun_get_theme_mod( 'fun-font-body-text', 'arial,helvetica')); ?>;
        }


        a, a:hover, a:focus, a:visited, .site-main a, .site-main a:visited, .site-main a:active, .site-main a:hover, .site-main a:focus {
          color: <?php echo fun_get_theme_mod( 'fun-color-text-link', '#aaa' ); ?>;
        }
        footer a, footer a:hover, footer a:focus, footer a:visited   {
          color: <?php echo fun_get_theme_mod( 'fun-color-text-footer', '#aaa' ); ?>;
        }

        .page .container .call-to-action {
          background-color: <?php echo fun_get_theme_mod( 'fun-color-call-to-action'); ?>;
        }
        h1, h2, h3, h4, h5, h6 {
            font-family: <?php echo fun_font_family( fun_get_theme_mod( 'fun-font-heading', 'arial,helvetica')); ?>;
        }
        .title {
          font-family: <?php echo fun_font_family( fun_get_theme_mod( 'fun-font-title', 'arial,helvetica')); ?>;
        }
        hr {
          border-color: <?php echo fun_get_theme_mod( 'fun-color-rule-lines', '#aaa' ); ?>;
        }
        .button {
            font-family: <?php echo fun_font_family( fun_get_theme_mod( 'fun-font-button', 'arial,helvetica')); ?>;
        }

        .wp-caption {
          font-family: <?php echo fun_font_family( fun_get_theme_mod( 'fun-font-caption', 'arial,helvetica')); ?>;
        }
        .caption {
            font-family: <?php echo fun_font_family( fun_get_theme_mod( 'fun-font-caption', 'arial,helvetica')); ?>;
        }

        .site-footer {
            color: <?php echo fun_get_theme_mod( 'fun-color-text-footer'); ?>;
        }

        .site-footer {
            font-family: <?php echo fun_font_family( fun_get_theme_mod( 'fun-font-footer', 'arial,helvetica')); ?>;
        }
    </style>
    <?php

}
add_action( 'wp_head', 'fun_customize_css');


// change editor_style CSS dynamically for the admin editor
function fun_theme_editor_dynamic_styles( $mceInit ) {
    $styles = " \
    body.mce-content-body {  \
      background-color: ".fun_get_theme_mod( 'fun-color-background', '#fff' )."; \
      color:".fun_get_theme_mod( 'fun-color-body-text', '#000' )."; \
      font-family:".fun_font_family( fun_get_theme_mod( 'fun-font-body-text', 'arial,helvetica'))."; \
    } \
    body.mce-content-body h1, h2, h3, h4, h5, h6 { \
      font-family:".fun_font_family( fun_get_theme_mod( 'fun-font-heading', 'arial,helvetica'))."; \
    } \
    .wp-caption-dd { \
      font-family:".fun_font_family( fun_get_theme_mod( 'fun-font-caption', 'arial,helvetica'))."; \
      color:".fun_get_theme_mod( 'fun-body-textcolor', '#000' )."; \
    } \
    ";

    if ( isset( $mceInit['content_style'] ) ) {
        $mceInit['content_style'] .= ' ' . $styles . ' ';
    } else {
        $mceInit['content_style'] = $styles . ' ';
    }
    return $mceInit;
}
add_filter('tiny_mce_before_init','fun_theme_editor_dynamic_styles');


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function fun_customize_preview_js() {
    wp_enqueue_script( 'fun_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '4'.date( '.YmdGi'), true );
}

add_action( 'customize_preview_init', 'fun_customize_preview_js' );
