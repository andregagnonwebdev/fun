<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package fun
 */

get_header(); ?>

<div id="main-content">
  <main role="main">

		<?php
		while ( have_posts() ) : the_post();
		?>
      <div class="container-fluid portfolio">
      	<div class="container">
      		<div class="row content-area">
            &nbsp;<br />
      			<div class="col-xs-12">
              <?php
              $args = array(
                'prev_text' => '&laquo; Previous',
                'next_text' => 'Next &raquo;',
                );
              the_post_navigation( $args);
              ?>
            </div>
            &nbsp;<br />
          </div>
        </div>
      </div>
    <?php
      get_template_part( 'template-parts/content', 'portfolio-single' );


			// If comments are open or we have at least one comment, load up the comment template.
			//if ( comments_open() || get_comments_number() ) :
			//	comments_template();
			//endif;

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
