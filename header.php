<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fun
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

<!-- Google Analytics -->
<?php if ( !WP_DEBUG): ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-291665-25"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-291665-25');
</script>
<?php endif; ?>
<!-- MailMunch for Andre Gagnon Web Development -->
<!-- Paste this code right before the </head> tag on every page of your site. -->
<!-- <script src="//a.mailmunch.co/app/v1/site.js" id="mailmunch-script" data-mailmunch-site-id="322190" async="async"></script> -->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'fun' ); ?></a>

	<?php
	// check if the flexible content field has rows of data
	if( have_rows('flexible_header') ):
		     // loop through the rows of data
		    while ( have_rows('flexible_header') ) : the_row();

		        switch( get_row_layout()) {
							case 'small_header':
								get_template_part( 'template-parts/header-small' );
		          	break;
							default:
								get_template_part( 'template-parts/header-default' );
			        	break;
			      }
			  endwhile;

	else :
			    // no layouts found
					get_template_part( 'template-parts/header-small' );
		//			get_template_part( 'template-parts/header-shrink' );
	endif;
	?>
